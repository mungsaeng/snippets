#ifndef NUM_H
#define NUM_H

#include <string> // - Числа передаеются через строку
#include <vector> // - Числа реализованы на vector'e
#include <stdexcept> // - Для выкидывания ошибок

enum Sign { plus = 0, minus }; // - Знаки числа


// - Составные части числа
//   - знак enum (Sign)
//   - цифры числа vector
//   - его система счисления
//
// - Число записывается от меньшего разряда к большему, прим.:
// строка = "123", т.е. vector = [3][2][1] и sign = plus


class NUM
{
    std::vector <uint8_t> digit; // - Число
    Sign sgn = plus;             // - Знак

    const uint8_t notation = 10; // - Система счисления

    // - Проверки
    void digits_in_notation(const std::string &value) const; // - Число в данной системе счисления
    void good_sign(const std::string &value) const;          // - Нет неразберихи со знаком, либо -,+, ' ' и проверка чтобы был только 1 знак
    void different_notations(const uint8_t &notate) const;   // - Проверка одинаковая ли система счисления (иначе операции не производятся)

    public:
    // - Конструкторы
    NUM(const uint8_t &notate, const std::string &value) noexcept;
    NUM(const uint8_t &notate, const Sign &sign, const std::initializer_list<uint8_t> &values) noexcept;

    NUM(const NUM &tmp) noexcept;
    NUM(NUM && tmp) noexcept;

    NUM& operator=(const NUM &tmp) noexcept;
    NUM& operator=(NUM &&tmp) noexcept;

    ~NUM() = default;

    // - Перегружеенные операторы с присвоением
    NUM& operator+(const NUM &tmp) noexcept;
    NUM& operator-(const NUM &tmp) noexcept;
    NUM& operator*(const NUM &tmp) noexcept;
    NUM& operator/(const NUM &tmp);
    // - Инкремент/декремент префиксный/постфиксный
    NUM& operator++();
    NUM& operator++(int);
    NUM& operator--();
    NUM& operator--(int);
    // - Перегружеенные операторы без присвоения
    friend NUM operator+(const NUM &a, const NUM &b) noexcept;
    friend NUM operator-(const NUM &a, const NUM &b) noexcept;
    friend NUM operator*(const NUM &a, const NUM &b) noexcept;
    friend NUM operator/(const NUM &a, const NUM &b);
    // Операторы сравнения
    friend NUM operator==(const NUM &a, const NUM &b) noexcept;
    friend NUM operator!=(const NUM &a, const NUM &b) noexcept;
    friend NUM operator>(const NUM &a, const NUM &b) noexcept;
    friend NUM operator<(const NUM &a, const NUM &b) noexcept;
    friend NUM operator>=(const NUM &a, const NUM &b) noexcept;
    friend NUM operator<=(const NUM &a, const NUM &b) noexcept;
    // - Вспомогательные методы
    std::string toString() const noexcept; // - Перевод значения в строку

};

#endif // NUM_H
